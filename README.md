# sofi-project
# LearnHub
## Description
A platform for elearning.

## Features
* Login/Logout pages
* Signup page
* Courses Overview personalized for User
* Skills Overview
* Lookup User based on the Courses enrolled
* Extensive Search on the entire catalog
* Modular framework

## System Requirements
* Python 3.7.x+
* SQLite

## Dependencies
See [requirements.txt](https://gitlab.com/sofi_hackaton2021/sofidemoproject/blob/master/requirements.txt) for more information.

## Build Instructions
### Application

1. First clone the project locally and then go into the directory
  ```
  $ git clone https://gitlab.com/sofi_hackaton2021/sofidemoproject.git 
  $ cd sofidemoproject
  ```

2. Setup our virtual environment
  ```
  $ pip install virtualenv
  $ virtualenv .venv
  ```

3. Now lets activate virtual environment
  ```
  $ source .venv/bin/activate
  ```

4. Now lets install the libraries this project depends on.
  ```
  $ pip install -r requirements.txt
  ```

5. Prepare the django server
  ```
  (.venv) $ cd sofi_learn
  (.venv) $ python manage.py createsuperuser
  (.venv) $ python manage.py makemigrations
  (.venv) $ python manage.py migrate
  ```

6. Configure the django-server secrets
   `SECRET_KEY` in sofi_learn/settings.py
   and turn DEBUG flag off if running on production

7. Run the django server
  ```
  (.venv) $ python manage.py runserver 0.0.0.0:8000
  ```

8. Your webpage is live at https://localhost:8000
  

### Database (optional)
We are almost done! Just follow these instructions and the database will be setup for the application to use.

1. Load up your postgres and enter the console. Then to create our database, enter:
  ```
  # create database sofi_db;
  ```

2. To confirm it was created, run this line, you should see the database in the output
  ```
  # \l
  ```

3. Enter the database
  ```
  # \c sofi_db
  ```

4. If you haven’t created an administrator for your previous projects, create one now by entering:
  ```
  # CREATE USER django WITH PASSWORD '123password';
  # GRANT ALL PRIVILEGES ON DATABASE sofi_db to django;
  ```

5. Your database "sofi_db" is now setup with an admin user account "django" using the passowrd "123password”. 