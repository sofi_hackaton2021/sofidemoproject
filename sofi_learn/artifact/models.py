from django.db import models
from accounts.models import CustomUser
from accounts.models import Student
from accounts.models import Teacher
from django.core.validators import MinValueValidator, MaxValueValidator

# search functions
from django.db.models import Q

# Create your models here.
COURSE_CATEGORY_TYPES = (
    ('Fitness', 'Fitness'),
    ('Programming', 'Programming'),
    ('Managment', 'Managment'),
    ('Business', 'Business'),
    ('Behavioral', 'Behavioral'),
    ('Infrastructure', 'Infrastructure'),
)
SKILL_LEVELS = (
    ('Noob', 'Noob'),
    ('Beginner', 'Beginner'),
    ('Intermediate', 'Intermediate'),
    ('Expert', 'Expert'),
    ('Godlevel', 'Godlevel'),
)

class Course(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=127)
    sub_title = models.CharField(max_length=127)
    category = models.CharField(max_length=127, choices=COURSE_CATEGORY_TYPES, default='General')
    description = models.TextField(null=True)
    start_date = models.DateField(null=True)
    finish_date = models.DateField(null=True)
    students = models.ManyToManyField(Student)
    teacher = models.ForeignKey(Teacher, on_delete=None)

    def delete(self, *args, **kwargs):
        super(Course, self).delete(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'sl_courses'


class Lecture(models.Model):
    lecture_id = models.AutoField(primary_key=True)
    lecture_num = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1)],
        default=1
    )
    week_num = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1)],
        default=1
    )
    title = models.CharField(max_length=63, default='', null=True)
    description = models.TextField(default='', null=True)
    youtube_url = models.URLField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    course = models.ForeignKey(Course, on_delete=None)

    def delete(self, *args, **kwargs):
        super(Lecture, self).delete(*args, **kwargs)

    def __str__(self):
        return 'Week: ' + str(self.week_num) + ' Lecture: ' + str(self.lecture_num) + ' Title: ' + self.title;

    class Meta:
        db_table = 'sl_lectures'

class SkillManager(models.Manager):
    def search(self, query=None):
        qs = self.get_queryset()
        if query is not None:
            or_lookup = ( Q(title__icontains=query)
                          # | Q(details__icontains=query)
                        )
            qs = qs.filter(or_lookup).distinct()
        return qs


class Skill(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=54)
    level = models.CharField(max_length=60, choices=SKILL_LEVELS, default='Beginner')
    skill_masters = models.ManyToManyField(CustomUser)
    objects = SkillManager()

    def delete(self, *args, **kwargs):
        super(Skill, self).delete(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        # ordering = ('level'),
        db_table = 'sl_skills'

    # def get_absolute_url(self):
    #     return reverse('profile_traveler', kwargs={'userid': self.user.id})
    #
    # @property
    # def get_html_url(self):
    #     url = reverse('app_main:trip_edit', args=(self.id,))
    #     return f'<a href="{url}">{self.user} in {self.destination}</a>'
    #
    # @property
    # def get_detail_url(self):
    #     url = reverse('app_main:trip_detail', args=(self.id,))
    #     return f'<a href="{url}">{self.user} in {self.destination}</a>'

def get_skill_queryset(query=None):
    queryset = []
    queries = query.split(' ')
    for q in queries:
        skills = Skill.objects.filter(
            Q(title__icontains=q)
        ).distict()
        for skill in skills:
            queryset.append(skill)
    return list(set(query))
