from django.shortcuts import render
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from accounts.models import CustomUser
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
import json
import sys
sys.path.append("..")

from artifact.models import Skill


@login_required(login_url='/login')
def skills_page(request):
    course_list = Skill.objects.all()
    paginator = Paginator(course_list, 25)
    page = request.GET.get('page')
    try:
        skills = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        skills = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        courses = paginator.page(paginator.num_pages)

    # Create our student account which will build our registration around.
    # try:
    #      student = Student.objects.get(user=request.user)
    # except Student.DoesNotExist:
    #      student = Student.objects.create(user=request.user)

    # Only fetch teacher and do not create new teacher here.
    # try:
    #     teacher = Teacher.objects.get(user=request.user)
    # except Teacher.DoesNotExist:
    #     teacher = None

    return render(request, 'skills/list.html', {
        'skills' : skills,
        # 'student' : student,
        # 'teacher' : teacher,
        'user' : request.user,
        'tab' : 'skills',
        # 'local_css_urls' : settings.SB_ADMIN_2_CSS_LIBRARY_URLS,
        # 'local_js_urls' : settings.SB_ADMIN_2_JS_LIBRARY_URLS
    })

#
# @login_required()
# def enroll(request):
#     response_data = {'status' : 'failure', 'message' : 'unsupported request format'}
#     if request.is_ajax():
#         course_id = int(request.POST['course_id'])
#         student = Student.objects.get(user=request.user)
#         course = Course.objects.get(id=course_id)
#
#         # Lookup the course in the students enrollment history and if the
#         # student is not enrolled, then enroll them now.
#         try:
#             Course.objects.get(
#                 students__student_id=student.student_id,
#                 id=course_id
#             )
#         except Course.DoesNotExist:
#             course.students.add(student)
#         response_data = {'status' : 'success', 'message' : 'enrolled' }
#
#     return HttpResponse(json.dumps(response_data), content_type="application/json")