from django.contrib import admin
from .models import Course, Lecture, Skill

# Register your models here.
admin.site.register(Course)
admin.site.register(Lecture)
admin.site.register(Skill)