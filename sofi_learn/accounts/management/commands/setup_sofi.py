from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from accounts.models import CustomUser


class Command(BaseCommand):
    """
        Run in your console:
        $ python manage.py setup_sofi
    """
    help = 'Picks the top 9 courses with the highest student enrollment.'

    def handle(self, *args, **options):
        """
            Function will create the objects necessary for some of the UI
            elements in the landpage.
        """
        CustomUser.objects.all().delete()
        CustomUser.objects.create(
            id=1,
            username='krishna',
            full_name="Krishna Prasanth Guttula",
            role="Lead Developer",
            email="krishnaprasanth.g@gmail.com",
            is_student=False,
            is_staff = True
        )
        CustomUser.objects.create(
            id=2,
            username='basheer',
            full_name="Basheer Jani",
            role="Lead Designer",
            email="basheer@gmail.com",
            is_student=True,
        )
        CustomUser.objects.create(
            id=3,
            username='sailesh',
            full_name="Sailesh",
            role="Developer",
            email="sailesh@gmail.com",
            is_student=True,
        )