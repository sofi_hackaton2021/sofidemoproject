from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class CustomUser(AbstractUser):
    # add additional fields in here
    full_name = models.CharField(max_length=32, blank=True, null=True)
    team = models.CharField(max_length=32, blank=True, null=True)
    role = models.CharField(max_length=32, blank=True, null=True)
    # nickname = models.CharField(max_length=32, blank=True, null=True)
    is_student = models.BooleanField(default=False)
    # is_teacher = models.BooleanField(default=False)

    def __str__(self):
        return self.username

class Student(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username

class Teacher(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username
