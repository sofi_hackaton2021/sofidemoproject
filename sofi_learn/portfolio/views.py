from django.shortcuts import render
from operator import attrgetter
from artifact.models import Skill
from artifact.models import Course

def portfolio_view(request):
    context = {}
    query = ''
    if request.GET:
        query = request.GET['q']
        context['query'] = str(query)
    skills = sorted(Skill.objects.filter(title__icontains=query), key=attrgetter('title'))
    context['skills'] = skills
    courses = sorted(Course.objects.filter(title__icontains=query), key=attrgetter('title'))
    context['courses'] = courses

    return render(request, 'index.html', context)
# Create your views here.
