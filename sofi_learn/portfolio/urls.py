from django.conf.urls import re_path, include, url
import sys
sys.path.append("..")

from artifact.views import courses
from artifact.views import skills
from portfolio.views import portfolio_view


urlpatterns = [
    re_path('^', portfolio_view, name='portfolio'),
    re_path(r'^courses$', courses.courses_page),
    re_path(r'^skills$', skills.skills_page),
]